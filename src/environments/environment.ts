// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  fbAppID: '147624942698727',
  backendApiEndpoint: 'http://localhost:5000',
  lastfm_api_key: '2f9c841386fd4e2aa9281a9e81617680',
  domain: 'http://localhost:4200',
  dataSaveLocation: 'data'
};
