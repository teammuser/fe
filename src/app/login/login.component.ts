import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';

import { TextContentService } from '@services/text-content.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private socialAuthService: AuthService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    public textContentService: TextContentService
  ) {}

  ngOnInit() {
    const token = this.activatedRoute.snapshot.queryParamMap.get('token');
    const temp = this.userService.finaliseLastfmLogin(token);
    if (temp) {
      temp.subscribe(result => {
        this.userService.saveData(result);
      });
    }
  }

  loginGoogle() {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  loginFb() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe(user => {
      if (user) {
        this.userService.loginWithFb(user).subscribe(result => {
          result.fbToken = user.authToken;
          this.userService.saveData(result);
        });
        this.router.navigate(['/']);
      } else {
      }
    });
  }

  loginLastfm() {
    this.userService.loginLastfm();
  }
}
