import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material';

import { LoginRoutingModule } from './login-routing.module';

import { LoginComponent } from './login.component';

@NgModule({
  imports: [LoginRoutingModule, MatButtonModule, CommonModule],
  declarations: [LoginComponent]
})
export class LoginModule {}
