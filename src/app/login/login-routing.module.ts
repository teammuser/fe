import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { IsLogedoutGuard } from '@guards/is-logedout.guard';

import { LoginComponent } from './login.component';

const loginRoutes: Routes = [
  {
    component: LoginComponent,
    // canActivate: [IsLogedoutGuard],
    path: 'login'
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(loginRoutes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
