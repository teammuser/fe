import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';

import { environment } from '@environments/environment';

import { EntitiesRelationshipsModule } from './entities-relationships/entities-relationships.module';
import { PlaylistManagementModule } from './playlist-management/playlist-management.module';
import { TimelineViewModule } from './timeline-view/timeline-view.module';
import { HomescreenModule } from './homescreen/homescreen.module';
import { AppRoutingModule } from './app-routing.module';
import { SearchModule } from './search/search.module';
import { SharedModule } from '@shared/shared.module';
import { LoginModule } from './login/login.module';

import { AppComponent } from './app.component';

const fbLoginOptions = {
  scope:
    'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
  return_scopes: true,
  enable_profile_selector: true
};

const googleLoginOptions = {
  scope: 'profile email'
};

const config = new AuthServiceConfig([
  // {
  //   id: GoogleLoginProvider.PROVIDER_ID,
  //   provider: new GoogleLoginProvider("Google-OAuth-Client-Id")
  // },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.fbAppID)
  }
]);

@NgModule({
  imports: [
    EntitiesRelationshipsModule,
    PlaylistManagementModule,
    BrowserAnimationsModule,
    SharedModule.forRoot(),
    TimelineViewModule,
    HttpClientModule,
    HomescreenModule,
    BrowserModule,
    SearchModule,
    LoginModule,
    AppRoutingModule,
    SocialLoginModule.initialize(config)
  ],
  declarations: [AppComponent],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
