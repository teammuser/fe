import { Component, OnInit } from '@angular/core';

import { ArtistModel } from '@models/artist.model';
import { AlbumModel } from '@models/album.model';
import { SongModel } from '@models/song.model';

import { TextContentService } from '@services/text-content.service';
import { SuggestionsService } from '@services/suggestions.service';
import { LastfmService } from '@services/lastfm.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-homescrren',
  templateUrl: './homescreen.component.html',
  styleUrls: ['./homescreen.component.scss']
})
export class HomescreenComponent implements OnInit {
  artists: ArtistModel[];
  songs: SongModel[];
  sugestedSongs: SongModel[];
  displaySongs: SongModel[];

  constructor(
    private textContentService: TextContentService,
    private suggestionService: SuggestionsService,
    private lastfmService: LastfmService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.lastfmService.getTopArtists().subscribe(result => {
      this.artists = result;
    });
    this.lastfmService.getTopTracks().subscribe(result => {
      this.songs = result;
      if (!this.displaySongs) {
        this.displaySongs = result;
      }
    });
    if (this.userService.isLogedin) {
      this.suggestionService.getSongSuggestions().subscribe(result => {
        this.sugestedSongs = result;
        if (result.length) {
          this.displaySongs = this.sugestedSongs;
        }
      });
    }
  }
}
