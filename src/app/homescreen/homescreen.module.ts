import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatButtonModule } from '@angular/material';

import { HomescreenRoutingModule } from './homescreen-routing.module';
import { SharedModule } from '@shared/shared.module';

import { HomescreenComponent } from './homescreen.component';

@NgModule({
  imports: [
    HomescreenRoutingModule,
    MatButtonModule,
    MatCardModule,
    SharedModule,
    CommonModule
  ],
  declarations: [HomescreenComponent]
})
export class HomescreenModule {}
