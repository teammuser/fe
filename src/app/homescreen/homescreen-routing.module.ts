import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { IsLogedinGuard } from '@guards/is-logedin.guard';

import { HomescreenComponent } from './homescreen.component';

const homescreenRoutes: Routes = [
  {
    path: '',
    component: HomescreenComponent,
    canActivate: [IsLogedinGuard]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(homescreenRoutes)],
  exports: [RouterModule]
})
export class HomescreenRoutingModule {}
