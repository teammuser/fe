import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EntitiesRelationshipsComponent } from './entities-relationships.component';

const entitiesRelantionshipsRoutes: Routes = [
  {
    path: 'relationships',
    component: EntitiesRelationshipsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(entitiesRelantionshipsRoutes)],
  exports: [RouterModule]
})
export class EntitiesRelationshipsRoutingModule {}
