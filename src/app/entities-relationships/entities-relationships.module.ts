import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatChipsModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatFormFieldModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EntitiesRelationshipsRoutingModule } from './entities-relationships-routing.module';

import { EntitiesRelationshipsComponent } from './entities-relationships.component';

@NgModule({
  imports: [
    CommonModule,
    EntitiesRelationshipsRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatRadioModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule
  ],
  declarations: [EntitiesRelationshipsComponent]
})
export class EntitiesRelationshipsModule {}
