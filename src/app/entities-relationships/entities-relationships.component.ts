import { Component, OnInit } from '@angular/core';

import { LastfmService } from '@services/lastfm.service';
import { ArtistModel } from '@models/artist.model';
import { FormControl, FormGroup } from '@angular/forms';
import { RelationshipsService } from '@services/relationships.service';
import { Node, Network, DataSet } from 'vis';

@Component({
  selector: 'app-entities-relationships',
  templateUrl: './entities-relationships.component.html',
  styleUrls: ['./entities-relationships.component.scss']
})
export class EntitiesRelationshipsComponent implements OnInit {
  events;
  selectedEntities = [];
  data = new FormGroup({
    selectedEntity: new FormControl(),
    conectionSelect: new FormControl()
  });
  options: ArtistModel[];
  graphData;
  network;
  graph_options = {};

  constructor(
    private lastfmService: LastfmService,
    private relationshipsService: RelationshipsService
  ) {}

  ngOnInit() {}

  remove(artistId) {
    const index = this.selectedEntities.findIndex(elem => elem.mbid === artistId);
    this.selectedEntities.splice(index, 1);
  }

  reset() {
    this.selectedEntities = [];
  }

  showConections() {
    if (this.data.value.conectionSelect === 'artists') {
      this.relationshipsService
        .getArtistConnections(this.selectedEntities.map(elem => elem.mbid))
        .subscribe(response => {
          response.nodes = response.nodes.map(elem => {
            elem.label = elem.name;
            elem.size = 100;
            return elem;
          });
          response.edges = response.edges.map(elem => {
            elem.length = 80;
            return elem;
          });
          this.network = new Network(
            document.getElementById('graph'),
            response,
            this.graph_options
          );
        });
    } else {
      this.relationshipsService
        .getTrackConnections(this.selectedEntities.map(elem => elem.mbid))
        .subscribe(response => {
          response.nodes = response.nodes.map(elem => {
            elem.label = elem.name;
            elem.size = 100;
            return elem;
          });
          response.edges = response.edges.map(elem => {
            elem.length = 80;
            return elem;
          });
          this.network = new Network(
            document.getElementById('graph'),
            response,
            this.graph_options
          );
        });
    }
  }

  searchEntities(event) {
    if (this.data.value.conectionSelect === 'artists') {
      this.lastfmService.searchArtist(event.target.value, 5).subscribe(response => {
        this.options = response;
      });
    } else {
      this.lastfmService.searchTrack(event.target.value, 5).subscribe(response => {
        this.options = response;
      });
    }
  }

  selectEntity() {
    this.selectedEntities.push(this.data.controls.selectedEntity.value);
    this.data.controls.selectedEntity.setValue(null);
  }

  displayFn(elem) {
    return elem ? elem.name : elem;
  }
}
