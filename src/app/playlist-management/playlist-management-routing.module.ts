import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { IsLogedinGuard } from '@guards/is-logedin.guard';

import { PlaylistViewComponent } from './playlist-view/playlist-view.component';
import { PlaylistListComponent } from './playlist-list/playlist-list.component';
import { PlaylistManagementComponent } from './playlist-management.component';

const playlistManagementRoutes: Routes = [
  {
    path: 'playlists',
    component: PlaylistManagementComponent,
    canActivate: [IsLogedinGuard],
    children: [
      {
        path: '',
        component: PlaylistListComponent
      },
      {
        path: ':playlist_id',
        component: PlaylistViewComponent
      }
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(playlistManagementRoutes)],
  exports: [RouterModule]
})
export class PlaylistManagementRoutingModule {}
