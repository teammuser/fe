import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatFormFieldModule,
  MatButtonModule,
  MatDialogModule,
  MatInputModule,
  MatTableModule,
  MatIconModule,
  MatCardModule,
  MatSortModule
} from '@angular/material';

import { PlaylistManagementRoutingModule } from './playlist-management-routing.module';
import { SharedModule } from '@shared/shared.module';

import { NewPlaylistDialogComponent } from './new-playlist-dialog/new-playlist-dialog.component';
import { PlaylistViewComponent } from './playlist-view/playlist-view.component';
import { PlaylistListComponent } from './playlist-list/playlist-list.component';
import { PlaylistManagementComponent } from './playlist-management.component';

@NgModule({
  imports: [
    PlaylistManagementRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatSortModule,
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    PlaylistManagementComponent,
    NewPlaylistDialogComponent,
    PlaylistViewComponent,
    PlaylistListComponent
  ],
  entryComponents: [NewPlaylistDialogComponent]
})
export class PlaylistManagementModule {}
