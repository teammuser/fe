import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { PlaylistListModel } from '@models/playlist-list.model';

import { TextContentService } from '@services/text-content.service';
import { PlaylistService } from '@services/playlist.service';

import { NewPlaylistDialogComponent } from '../new-playlist-dialog/new-playlist-dialog.component';

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss']
})
export class PlaylistListComponent implements OnInit {
  playlists: PlaylistListModel[] = [];

  constructor(
    private textContentService: TextContentService,
    private playlistService: PlaylistService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.playlistService.getAllPlaylists().subscribe(result => {
      this.playlists = result;
    });
  }

  edit(id: number) {
    this.router.navigate(['playlists', id]);
  }

  openNewDialog() {
    const dialogRef = this.dialog.open(NewPlaylistDialogComponent, {
      data: { name: 'New Playlist' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.playlistService.createPlaylist(result).subscribe(playlist => {
          playlist.name = result;
          this.playlists.push(playlist);
        });
      }
    });
  }

  delete(id) {}
}
