import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { TextContentService } from '@services/text-content.service';
import { PlaylistService } from '@services/playlist.service';
import { PlaylistViewModel } from '@models/playlist-view.model';

@Component({
  selector: 'app-playlist-view',
  templateUrl: './playlist-view.component.html',
  styleUrls: ['./playlist-view.component.scss']
})
export class PlaylistViewComponent implements OnInit, AfterViewInit {
  displayedColumns = ['title', 'artist', 'album', 'relreaseYear', 'length', 'action'];
  playlistId;
  playlist: PlaylistViewModel;
  editMode: boolean = false;
  playlistTableData = new MatTableDataSource();
  playlistData = new FormGroup({
    name: new FormControl('Playlist No1', [Validators.required])
  });
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private textContentService: TextContentService,
    private playlistService: PlaylistService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.playlistId = this.activatedRoute.snapshot.paramMap.get('playlist_id');
    this.playlistService.getPlaylist(this.playlistId).subscribe(result => {
      this.playlistData.controls.name.setValue(result.name);
      this.playlist = result;
      const songs: { id: string; name: string; artist: string }[] = result.songs.map(
        elem => {
          return { id: elem.id, name: elem.name, artist: elem.artist.name };
        }
      );
      this.playlistTableData = new MatTableDataSource(songs);
    });
  }

  ngAfterViewInit() {
    this.playlistTableData.sort = this.sort;
  }

  toggleEdit() {
    if (this.editMode) {
      this.playlistService
        .renamePlaylist(this.playlistId, this.playlistData.value.name)
        .subscribe(result => {
          this.playlist.name = this.playlistData.value.name;
        });
    }
    if (this.playlistData.valid) {
      this.editMode = !this.editMode;
    }
  }
}
