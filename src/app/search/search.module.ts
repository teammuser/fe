import {
  MatTabsModule,
  MatDialogModule,
  MatButtonModule,
  MatListModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SearchRoutingModule } from './search-routing.module';

import { SearchComponent } from './search.component';
import { SelectPlaylistComponent } from './select-playlist/select-playlist.component';

@NgModule({
  imports: [
    CommonModule,
    SearchRoutingModule,
    MatTabsModule,
    MatListModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [SearchComponent, SelectPlaylistComponent],
  entryComponents: [SelectPlaylistComponent]
})
export class SearchModule {}
