import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { PlaylistListModel } from '@models/playlist-list.model';

import { PlaylistService } from '@services/playlist.service';

@Component({
  selector: 'app-select-playlist',
  templateUrl: './select-playlist.component.html',
  styleUrls: ['./select-playlist.component.scss']
})
export class SelectPlaylistComponent implements OnInit {
  playlists: PlaylistListModel[];
  selected;

  constructor(
    private playlistService: PlaylistService,
    public dialogRef: MatDialogRef<SelectPlaylistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.playlistService.getAllPlaylists().subscribe(result => {
      this.playlists = result;
    });
  }
}
