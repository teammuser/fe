import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ArtistModel } from '@models/artist.model';
import { AlbumModel } from '@models/album.model';
import { SongModel } from '@models/song.model';

import { TextContentService } from '@services/text-content.service';
import { LastfmService } from '@services/lastfm.service';
import { PlaylistService } from '@services/playlist.service';

import { SelectPlaylistComponent } from './select-playlist/select-playlist.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  artists: ArtistModel[];
  albums: AlbumModel[];
  songs: SongModel[];
  searchString: string;

  constructor(
    private playlistService: PlaylistService,
    private activatedRoute: ActivatedRoute,
    private lastfmService: LastfmService,
    private router: Router,
    public dialog: MatDialog,
    public textContentService: TextContentService
  ) {}

  ngOnInit() {
    this.searchString = this.activatedRoute.snapshot.queryParamMap.get('q');
    this.lastfmService.searchArtist(this.searchString, 30).subscribe(result => {
      this.artists = result;
    });
    this.lastfmService.searchAlbum(this.searchString, 30).subscribe(result => {
      this.albums = result;
    });
    this.lastfmService.searchTrack(this.searchString, 30).subscribe(result => {
      this.songs = result;
    });
  }

  addToPlaylist(index) {
    const dialogRef = this.dialog.open(SelectPlaylistComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.playlistService
          .addSongs(result, [this.songs[index].mbid])
          .subscribe(playlist => {});
      }
    });
  }
}
