import { Component, OnInit } from '@angular/core';

import { TextContentService } from '@services/text-content.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(public textContentService: TextContentService) {}

  ngOnInit() {}
}
