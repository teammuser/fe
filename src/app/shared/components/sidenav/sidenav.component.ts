import { Component, OnInit, HostListener } from '@angular/core';

import { SidenavControlService } from '@services/sidenav-control.service';
import { TextContentService } from '@services/text-content.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  showStatus: boolean;
  sidenavMode = 'side';

  constructor(
    private sidenavControlService: SidenavControlService,
    public textContentService: TextContentService
  ) {}

  ngOnInit() {
    this.decideSidenavStatus(window.innerWidth);
    this.sidenavControlService
      .getStatus()
      .subscribe(status => (this.showStatus = status));
  }

  closeSidenav() {
    this.sidenavControlService.hide();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.decideSidenavStatus(event.target.innerWidth);
  }

  private decideSidenavStatus(width) {
    width > 600
      ? (this.sidenavControlService.show(), (this.sidenavMode = 'side'))
      : (this.sidenavControlService.hide(), (this.sidenavMode = 'over'));
  }
}
