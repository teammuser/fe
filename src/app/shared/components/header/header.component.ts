import { SocialUser, AuthService } from 'angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from '@models/user.model';

import { SidenavControlService } from '@services/sidenav-control.service';
import { TextContentService } from '@services/text-content.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showUser: boolean;
  userData: SocialUser | UserModel;
  searchString: string;

  constructor(
    private sidenavControlService: SidenavControlService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    public textContentService: TextContentService
  ) {}

  ngOnInit() {
    this.userService.isLogedin().subscribe(result => (this.showUser = result));
    this.userService.userData().subscribe(result => (this.userData = result));
  }

  toggleSidenav() {
    this.sidenavControlService.toggleStatus();
  }

  search() {
    this.router.navigate(['search'], { queryParams: { q: this.searchString } });
  }

  logout() {
    this.authService.signOut();
    this.router.navigate(['login']);
  }

  openUserSettings() {}
}
