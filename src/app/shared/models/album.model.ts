export interface AlbumModel {
  image: string;
  mbid: string;
  name: string;
  url: string;
}
