import { ArtistModel } from '@models/artist.model';

export interface SongModel {
  artist: ArtistModel;
  image: string;
  mbid: string;
  name: string;
  url: string;
}
