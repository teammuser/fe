export interface ArtistTimelineModel {
  timeline: { date: string; title: string; text: string }[];
}
