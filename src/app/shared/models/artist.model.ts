export interface ArtistModel {
  image: string;
  mbid: string;
  name: string;
  url: string;
}
