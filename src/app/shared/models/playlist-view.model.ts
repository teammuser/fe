export interface PlaylistViewModel {
  name: string;
  uri: string;
  id: string;
  songs: { name: string; id: string; artist: { name: string; id: string } }[];
}
