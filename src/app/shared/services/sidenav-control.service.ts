import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class SidenavControlService {
  private status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  constructor() {}

  show() {
    this.changeStatus(true);
  }

  hide() {
    this.changeStatus(false);
  }

  getStatus(): Observable<boolean> {
    return this.status.asObservable();
  }

  toggleStatus() {
    this.changeStatus(!this.status.getValue());
  }

  private changeStatus(newStatus: boolean) {
    this.status.next(newStatus);
  }
}
