import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

import { ArtistModel } from '@models/artist.model';
import { AlbumModel } from '@models/album.model';
import { SongModel } from '@models/song.model';

@Injectable()
export class LastfmService {
  baseArtistRecomandationEndpoint = `${environment.backendApiEndpoint}/lastfm/artists`;
  baseTrackRecomandationEndpoint = `${environment.backendApiEndpoint}/lastfm/tracks`;
  baseAlbumRecomandationEndpoint = `${environment.backendApiEndpoint}/lastfm/albums`;
  searchArtistsEndpoint = `${this.baseArtistRecomandationEndpoint}/search`;
  searchTracksEndpoint = `${this.baseTrackRecomandationEndpoint}/search`;
  searchAlbumsEndpoint = `${this.baseAlbumRecomandationEndpoint}/search`;
  topArtistsEndpoint = `${this.baseArtistRecomandationEndpoint}/top`;
  topSongsEndpoint = `${this.baseTrackRecomandationEndpoint}/top`;

  constructor(private http: HttpClient) {}

  getTopArtists(limit: number = 15): Observable<ArtistModel[]> {
    const params = new HttpParams().set('limit', limit.toString());
    return this.http.get<ArtistModel[]>(this.topArtistsEndpoint, {
      params: params
    });
  }

  searchArtist(name: string, limit: number = 15): Observable<ArtistModel[]> {
    const params = new HttpParams().set('name', name).set('limit', limit.toString());
    return this.http.get<ArtistModel[]>(this.searchArtistsEndpoint, {
      params: params
    });
  }

  getSimilarArtists(mbid: string, limit: number = 15): Observable<ArtistModel[]> {
    const url = `${this.baseArtistRecomandationEndpoint}/${mbid}/similar`;
    const params = new HttpParams().set('limit', limit.toString());
    return this.http.get<ArtistModel[]>(url, { params: params });
  }

  getArtistsTopTracks(mbid: string, limit: number = 15): Observable<SongModel[]> {
    const url = `${this.baseArtistRecomandationEndpoint}/${mbid}/topTracks`;
    const params = new HttpParams().set('limit', limit.toString());
    return this.http.get<SongModel[]>(url, {
      params: params
    });
  }

  getTopTracks(limit: number = 15): Observable<SongModel[]> {
    const params = new HttpParams().set('limit', limit.toString());
    return this.http.get<SongModel[]>(this.topSongsEndpoint, {
      params: params
    });
  }

  searchTrack(name: string, limit: number = 15): Observable<SongModel[]> {
    const params = new HttpParams().set('name', name).set('limit', limit.toString());
    return this.http.get<SongModel[]>(this.searchTracksEndpoint, {
      params: params
    });
  }

  getSimilarTracks(mbid: string, limit: number = 15): Observable<SongModel[]> {
    const url = `${this.baseTrackRecomandationEndpoint}/${mbid}/similar`;
    const params = new HttpParams().set('name', name).set('limit', limit.toString());
    return this.http.get<SongModel[]>(url, {
      params: params
    });
  }

  searchAlbum(name: string, limit: number = 15): Observable<AlbumModel[]> {
    const params = new HttpParams().set('name', name).set('limit', limit.toString());
    return this.http.get<AlbumModel[]>(this.searchAlbumsEndpoint, {
      params: params
    });
  }

  getAlbumInfo(mbid: string, limit: number = 15): Observable<AlbumModel[]> {
    const url = `${this.baseAlbumRecomandationEndpoint}/${mbid}/info`;
    const params = new HttpParams().set('limit', limit.toString());
    return this.http.get<AlbumModel[]>(url, { params: params });
  }
}
