import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

import { PlaylistListModel } from '@models/playlist-list.model';
import { PlaylistViewModel } from '@models/playlist-view.model';

import { UserService } from '@services/user.service';

@Injectable()
export class PlaylistService {
  playlistEndpoint = `${environment.backendApiEndpoint}/playlists`;

  constructor(private userService: UserService, private http: HttpClient) {}

  getAllPlaylists(): Observable<PlaylistListModel[]> {
    return this.http.get<PlaylistListModel[]>(
      `${this.playlistEndpoint}/${this.userService.getData().userId}`
    );
  }

  getPlaylist(id: string): Observable<PlaylistViewModel> {
    return this.http.get<PlaylistViewModel>(
      `${this.playlistEndpoint}/${this.userService.getData().userId}/${id}`
    );
  }

  addSongs(playlistId: string, songIds: string[]): Observable<any> {
    return this.http.post(
      `${this.playlistEndpoint}/${this.userService.getData().userId}/${playlistId}`,
      { songs: songIds }
    );
  }

  createPlaylist(
    playlistName: string,
    songIds: string[] = []
  ): Observable<PlaylistListModel> {
    return this.http.post<PlaylistListModel>(
      `${this.playlistEndpoint}/${this.userService.getData().userId}`,
      { songs: songIds, name: playlistName }
    );
  }

  renamePlaylist(id: string, name: string): Observable<any> {
    return this.http.post(
      `${this.playlistEndpoint}/${this.userService.getData().userId}/${id}`,
      { name: name }
    );
  }
}
