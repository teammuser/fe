import { Injectable } from '@angular/core';

@Injectable()
export class TextContentService {
  readonly defaultUserImage = 'assets/default-user-profile.png';
  readonly search = 'Search';
  readonly applicationTitle = 'MuSer';
  readonly glimpses = 'Glimpses';
  readonly genre = 'Genre';
  readonly statistics = 'Statistics';
  readonly yourMusic = 'Your Music';
  readonly pageNotFoundMessage = 'The Page You are Looking For Was Not Found';
  readonly pageNotFoundSorry = 'Sorry!';
  readonly fbLogoBlue = 'assets/fb/FB-f-Logo__white_50.png';
  readonly fbLoginText = 'Continue with Facebook';
  readonly googleLogoDark = 'assets/google/btn_google_dark_normal_ios.svg';
  readonly googleLoginText = 'Sign in with Google';
  readonly playlistNamePlaceholder = 'Playlist Name';
  readonly playlistNameRequiredError = 'Playlist Name is required';
  readonly playlistListTitle = 'Your Playlists';
  readonly topArtists = 'Top Artists';
  readonly topSongs = 'Top Songs';

  constructor() {}
}
