import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '@environments/environment';

import { SongModel } from '@models/song.model';

import { UserService } from '@services/user.service';

@Injectable()
export class SuggestionsService {
  songSuggestionsEndpoint = `${environment.backendApiEndpoint}/suggestions/tracks`;

  constructor(private userService: UserService, private http: HttpClient) {}

  getSongSuggestions(): Observable<SongModel[]> {
    const params = new HttpParams().set('userId', this.userService.getData().userId);
    return this.http.get<SongModel[]>(this.songSuggestionsEndpoint, { params: params });
  }
}
