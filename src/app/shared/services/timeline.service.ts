import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { ArtistTimelineModel } from '@models/artist-timeline.model';

import { environment } from '@environments/environment';

@Injectable()
export class TimelineService {
  timelineArtistaEndpoint = `${environment.backendApiEndpoint}/timeline/artists`;

  constructor(private http: HttpClient) {}

  getTimeline(artistIds: string[]): Observable<ArtistTimelineModel> {
    return this.http.post<ArtistTimelineModel>(this.timelineArtistaEndpoint, {
      mbids: artistIds
    });
  }
}
