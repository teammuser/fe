import { AuthService, SocialUser } from 'angularx-social-login';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators/map';

import { environment } from '@environments/environment';

import { UserModel } from '@models/user.model';

import { TextContentService } from '@services/text-content.service';

@Injectable()
export class UserService {
  private loginStatus: BehaviorSubject<boolean> = new BehaviorSubject(true);
  private user: BehaviorSubject<SocialUser | UserModel> = new BehaviorSubject(
    null
  );
  fbLoginEndpoint = `${environment.backendApiEndpoint}/login/fb`;
  lastfmLoginEndpoint = `${environment.backendApiEndpoint}/login/lastfm`;

  constructor(
    private textContentService: TextContentService,
    private socialAuthService: AuthService,
    private http: HttpClient
  ) {
    this.socialAuthService.authState.subscribe(user => {
      this.user.next(user);
      this.loginStatus.next(!!user);
    });
  }

  loginWithFb(userData: SocialUser, userId: string = null): Observable<any> {
    return this.http.post(this.fbLoginEndpoint, userData);
  }

  isLogedin(): Observable<boolean> {
    return this.loginStatus.asObservable();
  }

  isLogedout(): Observable<boolean> {
    return this.loginStatus.asObservable().pipe(map(value => !value));
  }

  userData(): Observable<SocialUser | UserModel> {
    return this.user.asObservable();
  }

  loginLastfm() {
    window.location.href = `http://www.last.fm/api/auth/?api_key=${
      environment.lastfm_api_key
    }&cb=${environment.domain}/login`;
  }

  finaliseLastfmLogin(token: string): Observable<any> {
    const endpoint = this.getData()
      ? `${this.lastfmLoginEndpoint}/${this.getData().userId}`
      : this.lastfmLoginEndpoint;
    if (token) {
      return this.http.post(endpoint, { authToken: token });
    }
    return null;
  }

  saveData(data) {
    localStorage.setItem(environment.dataSaveLocation, JSON.stringify(data));
  }

  getData() {
    return JSON.parse(localStorage.getItem(environment.dataSaveLocation));
  }
}
