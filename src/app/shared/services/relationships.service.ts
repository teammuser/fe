import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RelationshipsService {
  trackRelashionshipsEndpoint = `${environment.backendApiEndpoint}/connections/tracks`;
  artistRelashionshipsEndpoint = `${environment.backendApiEndpoint}/connections/artists`;

  constructor(private http: HttpClient) {}

  getArtistConnections(mbids: string[]): Observable<any> {
    return this.http.post(this.artistRelashionshipsEndpoint, { mbids: mbids });
  }

  getTrackConnections(mbids: string[]): Observable<any> {
    return this.http.post(this.trackRelashionshipsEndpoint, { mbids: mbids });
  }
}
