import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {
  MatFormFieldModule,
  MatSidenavModule,
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatListModule,
  MatIconModule
} from '@angular/material';

import { SidenavControlService } from '@services/sidenav-control.service';
import { RelationshipsService } from '@services/relationships.service';
import { TextContentService } from '@services/text-content.service';
import { SuggestionsService } from '@services/suggestions.service';
import { TimelineService } from '@services/timeline.service';
import { PlaylistService } from '@services/playlist.service';
import { LastfmService } from '@services/lastfm.service';
import { UserService } from '@services/user.service';

import { IsLogedoutGuard } from '@guards/is-logedout.guard';
import { IsLogedinGuard } from '@guards/is-logedin.guard';

import { PageNotFoundComponent } from '@components/page-not-found/page-not-found.component';
import { SidenavComponent } from '@components/sidenav/sidenav.component';
import { FooterComponent } from '@components/footer/footer.component';
import { HeaderComponent } from '@components/header/header.component';

const providers = [
  SidenavControlService,
  RelationshipsService,
  TextContentService,
  SuggestionsService,
  PlaylistService,
  TimelineService,
  IsLogedoutGuard,
  IsLogedinGuard,
  LastfmService,
  UserService
];

const componentsList = [
  PageNotFoundComponent,
  SidenavComponent,
  HeaderComponent,
  FooterComponent
];

@NgModule({
  imports: [
    MatFormFieldModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    RouterModule,
    CommonModule,
    FormsModule
  ],
  declarations: componentsList,
  exports: componentsList
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: providers
    };
  }
}
