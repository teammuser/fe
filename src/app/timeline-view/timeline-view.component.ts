import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ArtistModel } from '@models/artist.model';

import { TimelineService } from '@services/timeline.service';
import { LastfmService } from '@services/lastfm.service';

@Component({
  selector: 'app-timeline-view',
  templateUrl: './timeline-view.component.html',
  styleUrls: ['./timeline-view.component.scss']
})
export class TimelineViewComponent implements OnInit {
  events;
  selectedArtists: ArtistModel[] = [];
  selectedArtist = new FormControl();
  options: ArtistModel[];

  constructor(
    private timelineService: TimelineService,
    private lastfmService: LastfmService
  ) {}

  ngOnInit() {}

  remove(artistId) {
    const index = this.selectedArtists.findIndex(elem => elem.mbid === artistId);
    this.selectedArtists.splice(index, 1);
  }

  searchArtists(event) {
    this.lastfmService.searchArtist(event.target.value, 5).subscribe(response => {
      this.options = response;
    });
  }

  selectArtist() {
    this.selectedArtists.push(this.selectedArtist.value);
    this.selectedArtist.setValue(null);
  }

  displayFn(elem) {
    return elem ? elem.name : elem;
  }

  showTimeline() {
    this.timelineService
      .getTimeline(this.selectedArtists.map(elem => elem.mbid))
      .subscribe(result => {
        this.events = result;
      });
  }
}
