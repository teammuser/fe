import { VerticalTimelineModule } from 'angular-vertical-timeline';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatChipsModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatFormFieldModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TimelineViewRoutingModule } from './timeline-view-routing.module';

import { TimelineViewComponent } from './timeline-view.component';

@NgModule({
  imports: [
    CommonModule,
    VerticalTimelineModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    TimelineViewRoutingModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule
  ],
  declarations: [TimelineViewComponent]
})
export class TimelineViewModule {}
