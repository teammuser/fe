import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TimelineViewComponent } from './timeline-view.component';

const timelineViewRoutes: Routes = [
  {
    path: 'timeline',
    component: TimelineViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(timelineViewRoutes)],
  exports: [RouterModule]
})
export class TimelineViewRoutingModule {}
